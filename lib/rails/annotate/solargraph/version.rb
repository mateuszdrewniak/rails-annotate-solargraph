# frozen_string_literal: true

module Rails
  module Annotate
    module Solargraph
      VERSION = '0.5.5'
    end
  end
end
